import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import Search from '../Search/Search';

import './Stepper.css';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '70%',
    },
    backButton: {
        marginRight: theme.spacing(1),
        marginLeft: '35%',
    },
    instructions: {
        marginLeft: '30%',
        fontSize: '30px',
        color: 'white',
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
    },
}));

function getSteps() {
    return ['Select samples', 'Compute Control Samples', 'Signature', 'Result'];
}

function getStepContent(stepIndex) {
    switch (stepIndex) {
        case 0:
            return 'Select a Cancer Type';
        case 1:
            return 'Compute Control Samples';
        case 2:
            return 'Signature';
        default:
            return 'Result';
    }
}

export default function HorizontalLabelPositionBelowStepper() {
    const classes = useStyles();
    const [activeStep, setActiveStep] = React.useState(0);
    const steps = getSteps();

    const handleNext = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    const handleReset = () => {
        setActiveStep(0);
    };

    return (
        <div className={classes.root} id='Stepper'>

            <Stepper activeStep={activeStep} alternativeLabel>
                {steps.map((label) => (
                    <Step key={label}>
                        <StepLabel>{label}</StepLabel>
                    </Step>
                ))}
            </Stepper>

            <div className="Button">
                {activeStep === steps.length ? (
                    <div className="StepButton">
                        <Typography className={classes.instructions}>All steps completed</Typography>
                        <Button variant="outlined" onClick={handleReset}>Reset</Button>
                    </div>
                ) : (
                        <div>
                            <Typography className={classes.instructions}>
                                {/* {getStepContent(activeStep)} */}
                                </Typography>
                            <div className="StepButton">
                                <Button
                                    variant="outlined"
                                    disabled={activeStep === 0}
                                    onClick={handleBack}
                                    className={classes.backButton}
                                    
                                >
                                    Back</Button>
                                <Button
                                    variant="outlined"
                                    onClick={handleNext}>
                                    {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                                </Button>
                            </div>
                        </div>
                    )}
            </div>
        </div>
    );
}
