import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Checkbox from '@material-ui/core/Checkbox';

const {useState} = require('react');

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  formControl: {
    margin: theme.spacing(3),
  },
}));

const CheckboxesGroup = (props) => {
  const classes = useStyles();


  // const handleChange = (event) => {

  // };

  
  // console.log(isFemale);
  // console.log(isMale);

  // const { Female, Male } = state;
  // const error = [Female, Male].filter((v) => v).length !== 2;

  return (
    <div className={classes.root}>
      <FormControl component="fieldset" className={classes.formControl}>
        <FormLabel component="legend">Gender</FormLabel>
        <FormGroup>
          <FormControlLabel
            control={<Checkbox checked={props.isMale} onChange={(e) => 
              props.onGenderChange({isMale: !props.isMale, isFemale: props.isFemale})} name="Male" />}
            label="Male"
          />
          <FormControlLabel
            control={<Checkbox checked={props.isFemale} onChange={(e) => 
              props.onGenderChange({isMale: props.isMale, isFemale: !props.isFemale})} name="Female" />}
            label="Female"
          />
        </FormGroup>
        
      </FormControl>
      
    </div>
  );
}

export default CheckboxesGroup;