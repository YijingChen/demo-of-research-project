
import React, { useState } from 'react';
import Data from '../../Data/Data.json';
import Table from '../Table/Table';
import FilterSelect from '../FilterSelect/FilterSelect';
import './Search.css';

import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';

import SearchIcon from '@material-ui/icons/Search';
import FilterListIcon from '@material-ui/icons/FilterList';

import Chip from '@material-ui/core/Chip';
import Autocomplete from '@material-ui/lab/Autocomplete';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import Portal from '@material-ui/core/Portal';
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    searchBar: {
        margin: theme.spacing(1),
        color: 'white',
    },
    root: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        '& > *': {
            margin: theme.spacing(1),
        },
    },
}));

function Search() {

    const [value, setValue] = useState([]);
    const [show, setShow] = useState(false);
    const [expanded, setExpanded] = useState(false);
    const [filterList, setFilterList] = useState(["Female", "Male"]);
    const [isMale, setIsMale] = useState(true);
    const [isFemale, setIsFemale] = useState(true);

    const container = React.useRef(null);
    const classes = useStyles();
    let uniqueCancerType = [];

    const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
    const checkedIcon = <CheckBoxIcon fontSize="small" />;

    /* Get distinct Cancer Type from JSON*/
    function getUniqueCancerType() {
        Data.forEach(key => {

            if (uniqueCancerType.indexOf(key.Cancer_Type) == -1) {
                uniqueCancerType.push(key.Cancer_Type);
            }
        });
    }

    getUniqueCancerType();

    function handleClick() {
        setShow(!show)
    }

    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };


    function handleGenderChange(feature) {
        setIsFemale(feature.isFemale);
        setIsMale(feature.isMale);
        if (feature.isFemale && feature.isMale) {
            setFilterList(["Female", "Male"]);
        } else if (feature.isFemale) {
            setFilterList(["Female"]);
        } else if (feature.isMale) {
            setFilterList(["Male"]);
        }else{
            setFilterList([]);
        }
    }

    return (
        <div className="Search">
            <div className="SearchBar">
                <div className={classes.searchBar}>
                    <Grid container spacing={1} alignItems="flex-end">
                        <Grid item>
                            <SearchIcon className="icon" />
                        </Grid>
                        <Grid item>
                            <Autocomplete
                                multiple
                                id="autocomplete"
                                value={value}
                                onChange={(event, newValue) => {
                                    setValue(newValue);
                                }}
                                options={uniqueCancerType}
                                getOptionLabel={(uniqueCancerType) => uniqueCancerType}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        variant="standard"
                                        id="TextField"
                                        label="Enter a Disease Name..."
                                    />
                                )}
                            />
                            {/* <div>{`value: ${value !== null ? `'${value}'` : 'null'}`}</div> */}
                        </Grid>

                        <Grid item>

                        </Grid>
                    </Grid>
                </div>
                <div className="FilterOption">

                    {value.length > 0 ?
                        (<div>
                            <Tooltip title="Filter list">

                                <IconButton aria-label="filter list">
                                    <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
                                        <AccordionSummary>
                                            <FilterListIcon />
                                            <Typography>Filter Options</Typography>
                                        </AccordionSummary>
                                        <AccordionDetails>
                                            <FilterSelect onGenderChange={handleGenderChange} isFemale={isFemale} isMale={isMale} />
                                        </AccordionDetails>
                                    </Accordion>
                                </IconButton>
                            </Tooltip>
                        </div>) : (<div></div>)}

                </div>
            </div>

            <div className="Table">
                <Table selectedType={value} Data={Data} filterList={filterList} />
            </div>




        </div>
    );

}

export default Search;
